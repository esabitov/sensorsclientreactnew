import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

//Components
import App from './views/App'
import Login from './views/Login'
import Dashboard from './views/Dashboard'
import Users from './views/Users'

const routes = (
  <Router history={browserHistory}>
    <Route path='/' component={App}>
      <IndexRoute component={ Dashboard } />
      <Route path='login' component={Login} />
      <Route path='dashboard' component={Dashboard}/>
      <Route path='users' component={Users}/>
      <Route path='logout' component={App}/>
    </Route>
  </Router>
)

export default routes