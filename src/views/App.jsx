import React from 'react'
import { browserHistory } from 'react-router';
import config from '../config'
import $ from 'jquery'

import { notification } from 'antd';

import Login from './Login'
import Dashboard from './Dashboard'
import Users from './Users'

export default class App extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      signup: false,
      loggedIn: false,
      serverTime: '',
      clientTime: '',
      sensors:[]
    }
    this.handleLogin = this.handleLogin.bind(this)
    this.setToken = this.setToken.bind(this)
    // this.toggleSignup = this.toggleSignup.bind(this)
    this.getToken = this.getToken.bind(this)
    this.setToken = this.setToken.bind(this)
    this.logOut = this.logOut.bind(this)
    this.loggedIn = this.loggedIn.bind(this)
    this.checkAuth = this.checkAuth.bind(this)   
    
  }

  openNotificationWithIcon (type, message, description) {
    notification[type]({
      message,
      description
    })
  }

  setToken (token) {
    localStorage.setItem('mappingJwtToken', token)
    this.setState({ loggedIn: true })
  }

  getToken () {
    const token = localStorage.getItem('mappingJwtToken')
    return token
  }

  logOut () {
    localStorage.removeItem('mappingJwtToken')
    this.setState({ loggedIn: false })
    browserHistory.push('/login');
  }

  loggedIn () {
    this.setState({
      loggedIn: !!this.getToken()
    })
  }

  handleLogin (e, userInfo) {
    $.ajax({
      context: this,
      // url: e.target.action,
      url: `${config.apiUrl}/auth/login`,
      data: userInfo,
      method: 'post',
      success: (res) => {
        if (res.success) {
          this.setToken(res.token)
          this.setState({ loggedIn: true })
          browserHistory.push('/dashboard');
        } else {
          console.log(res.message)
          this.openNotificationWithIcon('error', 'Authentication Failed', res.message)
        }
      },
      error: (err) => {
        console.log(err.message)
        this.openNotificationWithIcon('error', 'Authentication Failed', err.message)
      }
    })
  }

  checkAuth () {
    if(this.props.location.pathname === '/logout'){
      this.logOut();
      return;
    }
    let loggedIn = false
    const token = this.getToken()
    if (token) {
      $.ajax({
        url: `${config.apiUrl}/auth/verify`,
        method: 'post',
        data: {token: token},
        success: (res) => {
          if (res.email) {
            this.setState({loggedIn: true})
            // browserHistory.push('/dashboard');
          } else {
          }
        },
        error: (err) => {
          console.log(err)
          browserHistory.push('/login');
        }
      })
    } else {
      this.setState({loggedIn: false})
      browserHistory.push('/login');
    }
  }

  render () {
    return (
      <div class="app">
        { 
          this.props.children ? 
              React.cloneElement(this.props.children, Object.assign(
                {}, 
                this.state, {
                handleLogin: this.handleLogin, 
                }
              )) 
            : '' 
        }
      </div>
    )
  }

  componentWillMount(){
    this.checkAuth()    
  }
}
