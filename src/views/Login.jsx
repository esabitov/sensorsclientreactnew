import React, { Component } from 'react';
import config from '../config';

import Container from '../components/Container';
import Logo from '../components/Logo';

import { Button } from 'antd';

class Login extends Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.loginOrSignUpPath = this.loginOrSignUpPath.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    this.props.handleLogin(e, {
      email:this.refs.email.value,
      password:this.refs.password.value,
      // admin:1
    })
  }

  loginOrSignUpPath () {
    return this.props.signup ? `${config.apiUrl}/users` : `${config.apiUrl}/auth/login`
  }

  render() {
    return (
      <Container>
        {/*<form action={this.props.signup ? `${config.apiUrl}/users` : `${config.apiUrl}/auth/login`} onSubmit={this.handleSubmit} class='login-form'>*/}
        <form onSubmit={this.handleSubmit} class='login-form'>
          <div class="a-center">
            <Logo />
          </div>
          <div class="input-field">
            <input type="text" name="email" ref="email" />
            <label htmlFor="last_name">Email</label>
          </div>
          <div class="input-field">
            <input type="password" class="validate" name="password" ref="password" />
            <label htmlFor="last_name">Password</label>
          </div>
          { /*
            this.props.signup
              ? <div>
                <input type="checkbox" value="admin"/>Admin
                <button class='login-form-button'>
                  Sign up
                </button>
                {' '}Or <a onClick={this.props.toggleSignup}>Log In</a>
                </div>
              : <div>
                <button class='login-form-button'>
                  Login
                </button>
                {' '}Or <a onClick={this.props.toggleSignup}>Sign Up</a>
              </div>
          */}
          <div class="a-center">
            <button class="btn waves-effect waves-light" type="submit" name="action">
              Log In
              <i class="material-icons right">send</i>
            </button>
          </div>
        </form>
      </Container>
    );
  }
}

export default Login;