import React, { Component } from 'react';
import config from '../config'
import Sensor from '../components/Sensor';
import Page from '../components/Page';
// import { Navbar, NavItem } from 'react-materialize';

import { Layout, Menu, Icon } from 'antd';
const SubMenu = Menu.SubMenu;
const { Content, Sider } = Layout;

let ws;

class Dashboard extends Component { 

  constructor (props) {
    super(props)
    this.state = {
      sensors:[],
      secondaryMenuCollapsed: true,
      secondaryMenuMode: 'vertical'
    }
    this.onCollapse = this.onCollapse.bind(this);
  }

  onCollapse(){
    this.setState({
      secondaryMenuCollapsed: !this.state.secondaryMenuCollapsed,
      secondaryMenuMode: this.state.secondaryMenuCollapsed ? 'inline' : 'vertical',
    });
  }

  //Display client time
  // checkTime(i) {
  //   if (i < 10) {
  //     i = "0" + i;
  //   }
  //   return i;
  // }

  // startTime() {
  //   var today = new Date();
  //   var h = today.getHours();
  //   var m = today.getMinutes();
  //   var s = today.getSeconds();
  //   // add a zero in front of numbers<10
  //   m = this.checkTime(m);
  //   s = this.checkTime(s);
  //   this.setState({
  //     clientTime: h + ":" + m + ":" + s
  //   });
  //   var t = setTimeout(()=>{
  //     this.startTime()
  //   }, 1000);
  // }

  startWS(){
    ws = new WebSocket(config.wsUrl);
    ws.onopen = function(){
      // Web Socket is connected, send data using send()
      // ws.send("Message to send");
      console.log("Message is sent...");
    };
    ws.onmessage = (msg) => { 
      let data = JSON.parse(msg.data);
      this.setState({
        serverTime:data.serverTime,
        sensors:data.sensors  
      });
    };
    ws.onclose = function(){ 
      // websocket is closed.
      console.log("Connection is closed..."); 
    };
  }

  endWS(){
    ws.close();
    ws = null;
  }

  restartSensor(sensor_id){
    ws.send(JSON.stringify({action:'restartSensor',sensor_id:sensor_id}));
  }

  render(){
    let sensors = [];
    this.state.sensors.forEach((el,i)=>{
      sensors.push(<Sensor key={i} data={el} restartSensor={this.restartSensor}/>)
    });
    return(
      <Page
        isMobile={this.props.isMobile}
        mobileMenuOpen={this.props.mobileMenuOpen}
        toggleMobileMenuOpen={this.props.toggleMobileMenuOpen}
      >
        <Sider 
          class="secondary-menu"
          collapsible
          collapsed={this.state.secondaryMenuCollapsed}
          onCollapse={this.onCollapse}
        >
          <Menu
            mode={this.state.secondaryMenuMode}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            style={{ height: '100%' }}
          >
            <SubMenu key="sub1" title={<span><Icon type="user" /><span class="nav-text">subnav 1</span></span>}>
              <Menu.Item key="1">option1</Menu.Item>
              <Menu.Item key="2">option2</Menu.Item>
              <Menu.Item key="3">option3</Menu.Item>
              <Menu.Item key="4">option4</Menu.Item>
            </SubMenu>
            <SubMenu key="sub2" title={<span><Icon type="laptop" /><span class="nav-text">subnav 2</span></span>}>
              <Menu.Item key="5">option5</Menu.Item>
              <Menu.Item key="6">option6</Menu.Item>
              <Menu.Item key="7">option7</Menu.Item>
              <Menu.Item key="8">option8</Menu.Item>
            </SubMenu>
            <SubMenu key="sub3" title={<span><Icon type="notification" /><span class="nav-text">subnav 3</span></span>}>
              <Menu.Item key="9">option9</Menu.Item>
              <Menu.Item key="10">option10</Menu.Item>
              <Menu.Item key="11">option11</Menu.Item>
              <Menu.Item key="12">option12</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Content style={{ padding: '0 24px', minHeight: 280 }}>
          {
            sensors.length > 0 ? 
            (
              <div>
                <h1>Currently listing all sensors:</h1>
                <div>{sensors}</div>
              </div>
            ):(
              <h1>No sensors connected</h1>
            )
          }
        </Content>
      </Page>
    )
  }

  componentDidMount(){
    this.startWS();
  }

  componentWillUnmount(){
    this.endWS();
  }
}

export default Dashboard;
