import React, { Component } from 'react';
import config from '../config';
import Page from '../components/Page';
import UserTable from '../components/UserTable';

class Users extends Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit() {
    console.log('hi')
  }

  render() {
    return (
      <Page
        isMobile={this.props.isMobile}
        mobileMenuOpen={this.props.mobileMenuOpen}
        toggleMobileMenuOpen={this.props.toggleMobileMenuOpen}
      >
        <UserTable />
      </Page>
    );
  }
}

export default Users;