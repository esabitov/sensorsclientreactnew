import React from 'react';
import ReactDOM from 'react-dom';
// import config from './config';
import routes from './router.js';
import App from './views/App';
//SCSS
import './App.scss'

ReactDOM.render(
  routes,
  document.getElementById('root')
)