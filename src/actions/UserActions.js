import dispatcher from '../dispatcher';

export function createUser(user){
  dispatcher.dispatch({
    type: 'CREATE_USER',
    user
  })
}

export function deleteUser(id){
  dispatcher.dispatch({
    type: 'DELETE_USER',
    id
  })
}

export function updateUser(user,id){
  dispatcher.dispatch({
    type: 'UPDATE_USER',
    user,
    id
  })
}