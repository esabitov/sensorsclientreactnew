import { EventEmitter } from "events";
import config from '../config'
import dispatcher from "../dispatcher";

import { notification, Modal } from 'antd';
const confirm = Modal.confirm;

class UserStore extends EventEmitter {
  constructor() {
    super()
    this.token = localStorage.getItem('mappingJwtToken');
    this.users = []
    this.refresh()
  }

  openNotificationWithIcon (type, message, description) {
    notification[type]({
      message,
      description
    })
  }

  showConfirm(title,message,callback) {
    confirm({
      title: title,
      content: message,
      onOk() {
        callback();
      },
      onCancel() {},
      okText: 'OK',
      cancelText: 'Cancel'
    });
  }

  refresh(){
    $.ajax({
      url: `${config.apiUrl}/users/`,
      method: 'get',
      data: {token: this.token},
      success: (res) => {
        this.users = res.map((item)=>{
          return {
            id: item['id'],
            email: item['email'],
            password: 'xxxxxxxxx'
          }
        });
        this.emit('change');
        // console.log('res',res);
      },
      error: (err) => {
        // console.log('err',err);
      }
    })
  }

  getAll(){
    return this.users;
  }

  createUser(user){
    $.ajax({
      url: `${config.apiUrl}/users/`,
      method: 'POST',
      data: {
        token: this.token,
        user: user
      },
      success: (res) => {
        if(res.success === false){
          this.openNotificationWithIcon('error', 'Could not create new user', res.message);
        }else{
          res['password'] = 'xxxxxxxxx';
          this.users.push(res);
          this.openNotificationWithIcon('success', 'User created successfully', '');
        }
        this.emit('change');
      },
      error: (err) => {
        this.openNotificationWithIcon('error', 'Could not create new user', err);
        this.emit('change');
      }
    })
  }

  deleteUser(id){
    this.showConfirm(
      'Are you sure you want to delete this user?',
      'This will permanently remove the user from the database',
      ()=>{
      return new Promise((resolve, reject) => {
        $.ajax({
          url: `${config.apiUrl}/users/${id}`,
          method: 'DELETE',
          data: {
            token: this.token,
            user: {
              id: id
            }
          },
          success: (res) => {
            if(res.success === false){
              reject;
              this.openNotificationWithIcon('error', 'Could not delete user', res.message);
            }else{
              this.users = this.users.filter((item)=>{
                return item.id !== id
              });
              resolve;
              this.openNotificationWithIcon('success', 'User deleted successfully', '');
            }
            this.emit('change');
          },
          error: (err) => {
            reject;
            this.openNotificationWithIcon('error', 'Could not delete user', err);
          }
        })
      }).catch(() => console.log('Oops modal errors!'));
    })
  }

  updateUser(user,id){
    $.ajax({
      url: `${config.apiUrl}/users/${id}`,
      method: 'PUT',
      data: {
        token: this.token,
        user: user
      },
      success: (res) => {
        if(res.success === false){
          this.openNotificationWithIcon('error', 'Could not update user', res.message);
        }else{
          this.openNotificationWithIcon('success', 'User updated successfully', '');
        }
        this.emit('change');
      },
      error: (err) => {
        this.openNotificationWithIcon('error', 'Could not update user', err);
        this.emit('change');
      }
    })
  }

  handleActions(action){
    console.log('UserStore recieved an action',action);
    switch(action.type){
      case "CREATE_USER":
        this.createUser(action.user);
        break;
      case "DELETE_USER":
        this.deleteUser(action.id);
        break;
      case "UPDATE_USER":
        this.updateUser(action.user,action.id);
        break;
    }
  }
}

const userStore = new UserStore;

dispatcher.register(userStore.handleActions.bind(userStore));

export default userStore;