import React, { Component } from 'react';

class Sensor extends Component {
  restartSensor(){
    this.props.restartSensor(this.props.data.id)
  }
  render() {
    return (
      <div class="sensor card blue-grey darken-1">
        <div class="card-content white-text">
          <span class="card-title">ID: 1</span>
          Sensor Time: {this.props.data.sensorTime} 
        </div>
        <div class="card-action">
          <a class="waves-effect waves-light btn" onClick={this.restartSensor.bind(this)}>
            <i class="material-icons left">cloud</i>
            Restart
          </a>
        </div>
      </div>
    );
  }
}

export default Sensor;
