import React, { Component } from 'react';

class Container extends Component {
  render() {
    return (
      <div class={"table align-center " + this.props.class}>
        <div class="cell">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Container;
