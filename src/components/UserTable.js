import React, {Component} from 'react'
import { Table, Input, Icon, Button, Popconfirm } from 'antd';

import UserStore from '../stores/UserStore';
import * as UserActions from '../actions/UserActions';

class EditableCell extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
      editable: this.props.editable || false
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.editable !== this.state.editable) {
      this.setState({ editable: nextProps.editable });
      if (nextProps.editable) {
        this.cacheValue = this.state.value;
      }
    }
    if (nextProps.status && nextProps.status !== this.props.status) {
      if (nextProps.status === 'save') {
        this.props.onChange(this.state.value);
      } else if (nextProps.status === 'cancel') {
        this.setState({ value: this.cacheValue });
        this.props.onChange(this.cacheValue);
      }
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.editable !== this.state.editable ||
           nextState.value !== this.state.value;
  }
  handleChange(e) {
    const value = e.target.value;
    this.setState({ value });
    this.props.onChange(value);
  }
  render() {
    const { value, editable } = this.state;
    return (<div>
      {
        editable ?
        <div>
          <Input
            value={value}
            onChange={e => this.handleChange(e)}
            id={this.props.id}
          />
        </div>
        :
        <div class="editable-row-text">
          {value.toString() || ' '}
        </div>
      }
    </div>);
  }
}

export default class UserTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      users: this.getAllUsers()
    }
    this.columns = [{
      title: 'ID',
      dataIndex: 'id',
      render: (text, record, index) => this.renderColumns(this.state.users, index, 'id', text),
    }, {
      title: 'Email',
      dataIndex: 'email',
      render: (text, record, index) => this.renderColumns(this.state.users, index, 'email', text),
    }, {
      title: 'Password',
      dataIndex: 'password',
      render: (text, record, index) => this.renderColumns(this.state.users, index, 'password', text),
    }, {
      title: 'Operation',
      dataIndex: 'operation',
      // fixed: 'right',
      render: (text, record, index) => {
        const { editable } = this.state.users[index].email;
        return (<div class="editable-row-operations">
          {
            editable ?
            <span>
              <a onClick={() => this.editDone(index, 'save')}>Save</a>
              <Popconfirm okText="Ok" cancelText="Cancel" title="Sure to cancel?" onConfirm={() => this.editDone(index, 'cancel')}>
                <a>Cancel</a>
              </Popconfirm>
            </span>
            :
            <span>
              <a onClick={() => this.edit(index)}>Edit</a>
            </span>
          }
          {
            !editable &&
              <span>
                <a onClick={() => this.delete(index)}>Delete</a>
              </span>
          }
        </div>);
      },
    }];
  }

  getAllUsers(){
    return UserStore.getAll().map((item) => {
      const obj = {};
      Object.keys(item).forEach((key) => {
        if(key === 'id'){
          obj[key] = item[key]
          obj['key'] = item[key]
        }else{
          obj[key] = {value : item[key], editable: false };
        }
      });
      return obj;
    });
  }

  renderColumns(data, index, key, text) {
    const { editable, status } = data[index][key];
    if (typeof editable === 'undefined') {
      return text;
    }
    return (<EditableCell
      editable={editable}
      value={text.value ? text.value : ''}
      onChange={value => this.handleChange(key, index, value)}
      status={status}
      id={key+'-'+data[index].siteId}
    />);
  }

  handleChange(key, index, value) {
    const data = this.state.users;
    data[index][key].value = value;
    this.setState({ data });
  }

  edit(index) {
    const data = this.state.users;
    Object.keys(data[index]).forEach((item) => {
      if (data[index][item] && typeof data[index][item].editable !== 'undefined') {
        data[index][item].editable = true;
      }
    });
    this.setState({ data });
  }

  editDone(index, type) {
    const data = this.state.users;
    let id = data[index].id;
    Object.keys(data[index]).forEach((item) => {
      if (data[index][item] && typeof data[index][item].editable !== 'undefined') {
        data[index][item].editable = false;
        data[index][item].status = type;
      }
    });
    if(id == 0){
      if(type == 'save'){ //save new user
        UserActions.createUser({
          email:data[index]['email'].value,
          password:data[index]['password'].value
        });
      }else{ //remove new add
        this.setState({
          users: this.state.users.filter((_, i) => i !== index)
        })
      }
    }else{
      if(type == 'save'){
        //save existing user
        UserActions.updateUser({
          email:data[index]['email'].value,
          password:data[index]['password'].value
        },id);
      }
    }
    this.setState({ data });
  }

  delete(index){
    UserActions.deleteUser(this.state.users[index].id);
  }

  handleAdd() {
    if(this.state.users.filter((e) => {return e.id === 0}).length === 0){
      this.setState({
        users: this.state.users.concat({
          email:{
            editable:true,
            value:'your@email.com'
          },
          password:{
            editable:true,
            value:'password'
          },
          key:0,
          id:0
        })
      })
    }
  }
  render() {
    const columns = this.columns;
    return (<div class="user-table">
      <a class="waves-effect waves-light btn add-user-btn" onClick={this.handleAdd.bind(this)}><i class="material-icons left">library_add</i>New User</a>
      <Table 
        dataSource={this.state.users} 
        columns={columns} 
        size='small' 
        locale={{
          filterConfirm: 'Ok',
          filterReset: 'Reset',
          emptyText: 'No Data',
        }}
      />
    </div>);
  }

  componentWillMount(){
    UserStore.on('change',()=>{
      this.setState({
        users: this.getAllUsers()
      });
    });
  }
}
