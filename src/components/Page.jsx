import React, { Component } from 'react';

import NavBar from '../components/NavBar';
import MainHeader from '../components/MainHeader';
import { Layout, Breadcrumb } from 'antd';
const { Content, Footer } = Layout;

import ReactTouchEvents from "react-touch-events";

class Page extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isMobile: window.innerWidth <= 767,
      mobileMenuOpen: false
    }
    this.handleWindowResize = this.handleWindowResize.bind(this)
    this.handleSwipe = this.handleSwipe.bind(this)
    this.toggleMobileMenuOpen = this.toggleMobileMenuOpen.bind(this)
  }

  handleWindowResize(){
    this.setState({
      isMobile: window.innerWidth <= 767,
      mobileMenuOpen: window.innerWidth <= 767 ? this.state.mobileMenuOpen : false
    });
  }

  handleSwipe(direction){
    if(this.state.isMobile){
      if(direction === 'left' && !this.state.mobileMenuOpen){
        //open menu
        this.setState({
          mobileMenuOpen:true
        });
      }
      if(direction === 'right' && this.state.mobileMenuOpen){
        //close menu
        this.setState({
          mobileMenuOpen:false
        });
      }
    }
  }

  toggleMobileMenuOpen(){
    this.setState({
      mobileMenuOpen: !this.state.mobileMenuOpen
    })
  }

  render() {
    return (
      <ReactTouchEvents
        onSwipe={this.handleSwipe.bind(this)}
      >  
        <div class={"page" + (this.state.mobileMenuOpen ? " responsive-menu-open" : "")}>
          <Layout class="ant-main-layout">
            <MainHeader 
              isMobile={this.state.isMobile}
              mobileMenuOpen={this.state.mobileMenuOpen}
              toggleMobileMenuOpen={this.toggleMobileMenuOpen}
            />
            <Content class="main-wrap">
              <Breadcrumb style={{ margin: '12px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>List</Breadcrumb.Item>
                <Breadcrumb.Item>App</Breadcrumb.Item>
              </Breadcrumb>
              <Layout style={{ padding: '24px 0', background: '#fff' }}>
                {this.props.children}
              </Layout>
            </Content>
            <Footer style={{ textAlign: 'center' }}>
              Mario Sensors | © 2017
            </Footer>
          </Layout>
          {
            this.state.isMobile && (
              <NavBar logOut={this.props.logOut} mobileMenuOpen={this.state.mobileMenuOpen}/>
            )
          }
        </div>
      </ReactTouchEvents>
    );
  }

  componentDidMount(){
    window.addEventListener('resize', this.handleWindowResize);
  }

  componentWillUnmount(){
    window.removeEventListener('resize', this.handleWindowResize);
  }
}

export default Page;
