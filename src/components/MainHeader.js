import React, { Component } from 'react';
import NavBar from '../components/NavBar';
import Logo from '../components/Logo';
import { Menu } from 'antd';

import { Layout, Icon } from 'antd';
const { Header } = Layout;

class MainHeader extends Component {

  constructor(props){
    super(props);
    this.toggleMobileMenuOpen = this.toggleMobileMenuOpen.bind(this);
  }

  toggleMobileMenuOpen() {
    this.props.toggleMobileMenuOpen();
  }

  render() {
    return (
      <Header class="header">
        <div class="main-wrap table">
          <div class="cell">
            <Logo />
          </div>
          <div class="cell a-right">
            {this.props.isMobile ? (
              <Icon type={this.props.mobileMenuOpen ? "menu-unfold" : "menu-fold"} onClick={this.toggleMobileMenuOpen}/>
            ) : (
              <NavBar />
            )}            
          </div>
        </div>
      </Header>
    );
  }
}

export default MainHeader;
