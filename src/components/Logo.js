import React, { Component } from 'react';

class Logo extends Component {
  render() {
    return (
      <div class="logo">
        L.O.G.O
      </div>
    );
  }
}

export default Logo;
