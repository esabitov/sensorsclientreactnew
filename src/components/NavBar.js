import React, { Component } from 'react';
import { Navigation, Link, browserHistory } from 'react-router'
import { Menu } from 'antd';

class NavBar extends Component {

  render() {
    return (
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['2']}
      >
        <Menu.Item key="1">
          <Link to="/dashboard">Dashboard</Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/users">Users</Link>
        </Menu.Item>
        <Menu.Item key="5">
          <Link to="/logout">Logout</Link>
        </Menu.Item>
      </Menu>
    );
  }
}

export default NavBar;
